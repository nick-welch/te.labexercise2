
#include <conio.h>
#include <iostream>
#include <string>

using namespace std;


enum Rank
{
	Two = 2, Three = 3, Four = 4, Five = 5, Six = 6, Seven = 7, Eight = 8, Nine = 9, Ten = 10, Jack = 11, Queen = 12, King = 13, Ace = 14
};

enum Suit
{
	Hearts, Clubs, Diamonds, Spades
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) 
{
	
	

	switch (card.rank) 
	{
	case 2:
		cout << "The two of ";
		break;
	case 3:
		cout << "The three of ";
		break;
	case 4: 
		cout << "The Four of ";
		break;
	case 5: 
		cout << "The Five of ";
		break;
	case 6: 
		cout << "The Six of ";
		break;
	case 7: 
		cout << "The Seven of ";
		break;
	case 8: 
		cout << "The eight of ";
		break;
	case 9: 
		cout << "The Nine of ";
		break;
	case 10: 
		cout << "The Ten of ";
		break;
	case 11: 
		cout << "The Jack of ";
		break;
	case 12: 
		cout << "The Queen of ";
		break;
	case 13: 
		cout << "The King of ";
		break;
	default: cout << "The Ace of ";
	}

	switch (card.suit) {
	case 0: 
		cout << "hearts \n";
		break;
	case 1: 
		cout << "Clubs \n";
		break;
	case 2: 
		cout << "Diamonds \n";
		break;
	default: 
		cout << "Spades \n";
		break;
	}
	
	
}

void HighCard(Card card1, Card card2) 
{
	if(card1.rank > card2.rank)
	{
		cout << "The first card has the higher value";
	}
	else if (card1.rank < card2.rank)
	{
		cout << "The second card has the higher value";
	}
	else 
	{
		cout << "Both cards have the same value";
	}
}

int main()
{

	Card c;
	c.rank = Queen;
	c.suit = Diamonds;
	PrintCard(c);

	Card c2;
	c2.rank = Ace;
	c2.suit = Spades;
	PrintCard(c2);

	char ch = _getch();
	ch;
	
	return 0;
}